CREATE TABLE users (
    user_id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL,
    parent_id INT,
    FOREIGN KEY (parent_id) REFERENCES users(user_id)
);

 INSERT INTO users (username, parent_id)
    VALUES
      ('user1', null),
      ('user2', 1),
      ('user3', 1),
      ('user4', 2);
