<?php
include 'api_functions.php';

$install = isset($_GET['install']) ? $_GET['install'] : null;
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$filter = isset($_GET['filter']) ? $_GET['filter'] : null;

if ($install == 1) {
    createUsersTable();
    addUsers();
}

$usersData = getUsers($page, $filter);

header('Content-Type: application/json');
echo json_encode($usersData);
?>
