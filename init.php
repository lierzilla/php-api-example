<?php
include 'api_functions.php';

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$filter = isset($_GET['filter']) ? $_GET['filter'] : null;

$usersData = getUsers($page, $filter);
header('Content-Type: application/json');
echo json_encode($usersData);
?>
