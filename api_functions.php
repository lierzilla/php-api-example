<?php
include 'config.php';

function getUsers($page = 1, $filter = null) {
    global $conn;
    $limit = 10;

    $sql = "SELECT u.*, p.username AS parent_username
            FROM users u
            LEFT JOIN users p ON u.parent_id = p.user_id";

    if ($filter) {
        $sql .= " WHERE u.username LIKE '%$filter%'";
    }

    $offset = ($page - 1) * $limit;
    $sql .= " LIMIT $limit OFFSET $offset";

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $data = array();

        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }

        $nextPageData = getUsers($page + 1, $filter);
        $data = array_merge($data, $nextPageData);

        return $data;
    } else {
        return array();
    }
}

function createUsersTable() {
    global $conn;

    $sql = "
        CREATE TABLE IF NOT EXISTS users (
        user_id INT AUTO_INCREMENT PRIMARY KEY,
        username VARCHAR(255) NOT NULL,
        parent_id INT DEFAULT NULL,
        FOREIGN KEY (parent_id) REFERENCES users(user_id)
        );
    ";

    $result = $conn->query($sql);

    if ($result) {
        return array("message" => "Success");
    } else {
        return array("error" => "Error while creating table: " . $conn->error);
    }
}

function addUsers() {
    global $conn;

    $sql = "
        INSERT INTO users (username, parent_id)
        VALUES
        ('user1', null),
        ('user2', 1),
        ('user3', 1),
        ('user4', 2);
    ";

    $result = $conn->query($sql);

    if ($result) {
        return array("message" => "Success");
    } else {
        return array("error" => "Error while creating table: " . $conn->error);
    }
}

?>
