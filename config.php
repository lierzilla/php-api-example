<?php
$servername = getenv('SERVERNAME');
$username = getenv('DBUSER');
$password = getenv('PASS');
$dbname = getenv('DBNAME');

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
die("Connection failed: " . $conn->connect_error);
}

?>
